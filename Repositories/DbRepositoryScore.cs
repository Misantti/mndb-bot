﻿using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using mndb_bot.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        public async Task<Score> GetUserScore(User user, Movie movie)
        {
            return await _context.Scores.FirstOrDefaultAsync(item => item.UserId == user.Id && item.MovieId == movie.Id);

        }
        public async Task<List<Score>> GetUserScores(User user)
        {
            return await _context.Scores.Where(item => item.UserId == user.Id).ToListAsync();
        }
        public async Task<bool> SetUserScore(Score score)
        {
            ScoreValidator validator = new();
            var results = validator.Validate(score);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
                return false;
            }

            if (await GetMovie(score.MovieId) == null || await GetUser(score.UserId) == null)
            {
                return false;
            }
            var existingScore = await _context.Scores.FirstOrDefaultAsync(item => item.UserId == score.UserId && item.MovieId == score.MovieId);
            if (existingScore == null)
            {
                _context.Add(score);
                return await TrySaveChangesAsync();
            }
            else
            {
                _context.Entry(existingScore).State = EntityState.Detached;
                existingScore.ScoreVal = score.ScoreVal;
                _context.Attach(existingScore);
                _context.Scores.Update(existingScore);
                return await TrySaveChangesAsync();
            }
        }
        public async Task<double> GetScore(Movie movie)
        {
            var scores = await _context.Scores.Where(item => item.MovieId == movie.Id && item.ScoreVal > 0).ToArrayAsync();

            if (scores.Count() > 0)
            {
                double score = 0;
                foreach (Score item in scores)
                {
                    score += Convert.ToDouble(item.ScoreVal);
                }
                return score / scores.Count();
            }
            else
            {
                return 0;
            }
        }
    }
}
