﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mndb_bot.Contexts;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        private readonly BotDBContext _context;
        public DbRepository(BotDBContext context)
        {
            _context = context;
        }
        public async Task<bool> TrySaveChangesAsync()
        {
            bool result;
            try
            {
                await _context.SaveChangesAsync();
                result = true;
            }
            catch(DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
                result = false;
            }
            return result;
        }

    }
}