﻿using mndb_bot.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{    public partial interface IDbRepository //user
    {
        Task<User> GetUser(int id);
        Task<List<User>> GetUsers();
        Task<User> GetUserByDiscordID(ulong discordID);
        Task<User> GetUserByName(string userName);
        Task<User> CreateUser(User user);
        Task<User> ChangeRole(User user, string role);
        Task<bool> DeleteUser(User user);
    }
    public partial interface IDbRepository //genre
    {
        Task<bool> CreateGenre(Genre genre);
        Task AddGenreToMovie(MoviesGenres mg);
        Task<List<Genre>> GetGenres();
        Task<bool> GenreChangeEmoji(Genre genre, string emoji);
        Task<Genre> GetGenre(int id);
        Task<Genre> GetGenreByName(string name);
        Task DeleteGenre(Genre genre);
        Task<List<Genre>> GetMoviesGenres(Movie movie);
    }
    public partial interface IDbRepository //movie
    {
        Task<Movie> GetMovie(int id);
        Task<Movie> CreateMovie(Movie movie);
        Task<Movie> GetMovieByIMDB(string imdb);
        Task<Movie> GetMovieByTMDB(int tmdb);
        Task<List<Movie>> GetMovies();
        Task<List<Movie>> GetMoviesForEvent(Event e);
        Task<bool> SetEventForMovie(Movie movie, Event e);
        Task<bool> DeleteMovie(Movie movie);
    }
    public partial interface IDbRepository //event
    {
        Task<Event> GetEvent(int id);
        Task<Event> GetEventByDate(DateTime time);
        Task<Event> CreateEvent(Event e);
        Task<List<Event>> GetEvents();
        Task<List<Event>> GetMoviesEvents(Movie movie);
        Task<bool> DeleteEvent(Event e);
    }
    public partial interface IDbRepository //scores
    {
        Task<Score> GetUserScore(User user, Movie movie);
        Task<List<Score>> GetUserScores(User user);
        Task<bool> SetUserScore(Score score);
        Task<double> GetScore(Movie movie);
    }
}
