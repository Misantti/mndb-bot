﻿using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using mndb_bot.Validators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        public async Task<User> GetUser(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(item => item.Id == id);
        }
        public async Task<List<User>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }
        public async Task<User> GetUserByDiscordID(ulong discordID)
        {
            return await _context.Users.FirstOrDefaultAsync(item => item.DiscordId == discordID);
        }
        public async Task<User> GetUserByName(string userName)
        {
            return await _context.Users.FirstOrDefaultAsync(item => item.Username.ToLower() == userName.ToLower());
        }
        public async Task<User> CreateUser(User user)
        {
            UserValidator validator = new();
            var results = validator.Validate(user);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
                return null;
            }
            _context.Users.Add(user);
            bool success = await TrySaveChangesAsync();
            if(!success)
            {
                return null;
            }
            return await GetUserByDiscordID(user.DiscordId);
        }
        public async Task<User> ChangeRole(User user, string role)
        {
            var existingUser = await GetUserByDiscordID(user.DiscordId);
            _context.Entry(existingUser).State = EntityState.Detached;
            existingUser.Role = role;
            _context.Attach(existingUser);
            _context.Users.Update(existingUser);
            bool success = await TrySaveChangesAsync();
            if (!success)
            {
                return null;
            }
            return existingUser;
        }
        public async Task<bool> DeleteUser(User user)
        {
            var existingUser = await GetUser(user.Id);
            if (existingUser != null)
            {
                _context.Users.Remove(existingUser);
                return await TrySaveChangesAsync();
            }
            else
            {
                return false;
            }
        }
    }
}
