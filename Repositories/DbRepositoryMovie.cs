﻿using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using mndb_bot.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        public async Task<Movie> GetMovie(int id)
        {
            return await _context.Movies.FirstOrDefaultAsync(item => item.Id == id);
        }
        public async Task<Movie> GetMovieByIMDB(string imdb)
        {
            return await _context.Movies.FirstOrDefaultAsync(item => item.Imdb == imdb);
        }
        public async Task<Movie> GetMovieByTMDB(int tmdb)
        {
            return await _context.Movies.FirstOrDefaultAsync(item => item.Tmdb == tmdb);
        }
        public async Task<List<Movie>> GetMovies()
        {
            var allMovies = await _context.Movies.OrderBy(item => item.CreatedAt).ToListAsync();
            if (allMovies.Count == 0)
            {
                return null;
            }
            return allMovies;
        }
        public async Task<Movie> CreateMovie(Movie movie)
        {
            MovieValidator validator = new();
            var results = validator.Validate(movie);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
                return null;
            }
            if (await GetMovieByIMDB(movie.Imdb) == null)
            {
                _context.Movies.Add(movie);

                bool success = await TrySaveChangesAsync();
                if (success)
                {
                    return await GetMovieByIMDB(movie.Imdb);
                }
                else
                {
                    return null;
                }
            }
            
            return null;

        }

        public async Task<List<Movie>> GetMoviesForEvent(Event e)
        {
            var eventsmovies = await _context.Event_Movies
                .Where(item => item.EventId == e.Id)
                .ToListAsync();
            if (eventsmovies.Count == 0)
            {
                return null;
            }
            List<Movie> movies = new();
            foreach(EventMovies item in eventsmovies)
            {
                movies.Add(await GetMovie(item.MovieId));
            }
            if (movies.Count == 0)
            {
                return null;
            }
            return movies;
        }
        public async Task<bool> SetEventForMovie(Movie userMovie, Event userEvent)
        {
            var existingMovie = await GetMovie(userMovie.Id);
            var existingEvent = await GetEvent(userEvent.Id);
            if (existingMovie == null || existingEvent == null)
            {
                return false;
            }
            else
            {
                var existingConnections = _context.Event_Movies.Where(item => item.MovieId == userMovie.Id && item.EventId == userEvent.Id).ToArray();
                if (existingConnections.Length != 0)
                {
                    return false;

                }
                else
                {
                    var connection = new EventMovies()
                    {
                        EventId = userEvent.Id,
                        MovieId = userMovie.Id
                    };
                    _context.Add(connection);
                    return await TrySaveChangesAsync();
                }
            }
        }
        public async Task<bool> DeleteMovie(Movie movie)
        {
            var existingMovie = await GetMovie(movie.Id);
            if (existingMovie != null)
            {
                _context.Movies.Remove(existingMovie);
                return await TrySaveChangesAsync();
            }
            else
            {
                return false;
            }
        }
    }
}
