﻿using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using mndb_bot.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        public async Task<bool> CreateGenre(Genre genre)
        {
            GenreValidator validator = new();
            var results = validator.Validate(genre);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
                return false;
            }

            if (await _context.Genres.FirstOrDefaultAsync(item => item.Id == genre.Id) == null)
            {
                _context.Genres.Add(genre);
                var success = await TrySaveChangesAsync();
                if(success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else return false;
        }
        public async Task AddGenreToMovie(MoviesGenres mg)
        {
            MoviesGenresValidator validator = new();
            var results = validator.Validate(mg);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
            }
            else
            {
                if (await _context.Movies_Genres.FirstOrDefaultAsync(item => item.GenreId == mg.GenreId && item.MovieId == mg.MovieId) == null)
                {
                    _context.Movies_Genres.Add(mg);
                    await TrySaveChangesAsync();
                }
            }
        }
        public async Task<List<Genre>> GetGenres()
        {
            return await _context.Genres.ToListAsync();
        }
        public async Task<bool> GenreChangeEmoji(Genre genre, string emoji)
        {
            var existingGenre = await _context.Genres.FirstOrDefaultAsync(item => item.Id == genre.Id);
            if (existingGenre != null)
            {
                _context.Entry(existingGenre).State = EntityState.Detached;

                existingGenre.Emoji = emoji;

                _context.Attach(existingGenre);
                _context.Genres.Update(existingGenre);
                var success = await TrySaveChangesAsync();
                if (success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public async Task<Genre> GetGenre(int id)
        {
            Genre genre = await _context.Genres.FirstOrDefaultAsync(item => item.Id == id);
            if (genre != null)
            {
                return genre;
            }
            else return null;
        }
        public async Task<Genre> GetGenreByName(string name)
        {
            Genre genre = await _context.Genres.FirstOrDefaultAsync(item => item.Name.ToLower() == name.ToLower());
            if (genre != null)
            {
                return genre;
            }
            else return null;
        }
        public async Task DeleteGenre(Genre genre)
        {
            _context.Genres.Remove(genre);
            await TrySaveChangesAsync();
        }
        public async Task<List<Genre>> GetMoviesGenres(Movie movie)
        {
            var mgArray = await _context.Movies_Genres.Where(item => item.MovieId == movie.Id).ToArrayAsync();
            if (mgArray.Count() == 0)
            {
                return null;
            }
            else
            {
                List<Genre> genreList = new();
                foreach(MoviesGenres item in mgArray)
                {
                    genreList.Add(await GetGenre(item.GenreId));
                }
                return genreList;
            }

        }
    }
}
