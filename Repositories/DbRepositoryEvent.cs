﻿using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using mndb_bot.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Repositories
{
    public partial class DbRepository : IDbRepository
    {
        public async Task<Event> GetEvent(int id)
        {
            return await _context.Events.FirstOrDefaultAsync(item => item.Id == id);
        }
        public async Task<Event> GetEventByDate(DateTime time)
        {
            return await _context.Events.FirstOrDefaultAsync(item => item.Time.Date == time.Date);
        }
        public async Task<Event> CreateEvent(Event e)
        {
            EventValidator validator = new();
            var results = validator.Validate(e);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
                return null;
            }

            var existingEvent = await _context.Events.FirstOrDefaultAsync(item => item.Time.Date == e.Time.Date);
            if (existingEvent != null)
            {
                return null;
            }
            else
            {
                await _context.Events.AddAsync(e);
                var success = await TrySaveChangesAsync();
                if(success)
                {
                    return await GetEventByDate(e.Time);
                }
                else
                {
                    return null;
                }
            }
        }
        public async Task<List<Event>> GetEvents()
        {
            var allEvents = await _context.Events.OrderBy(item => item.Time).ToListAsync();
            if (allEvents.Count() == 0)
            {
                return null;
            }
            return allEvents;
        }
        public async Task<List<Event>> GetMoviesEvents(Movie movie)
        {
            var movieEvents = await _context.Event_Movies.Where(item => item.MovieId == movie.Id).ToArrayAsync();
            if (movieEvents.Count() == 0)
            {
                return null;
            }
            else
            {
                List<Event> returnList = new();
                foreach (EventMovies item in movieEvents)
                {
                    returnList.Add(await GetEvent(item.EventId));
                }
                return returnList;
            }
        }
        public async Task<bool> DeleteEvent(Event e)
        {
            var existingEvent = await GetEvent(e.Id);
            if (existingEvent != null)
            {
                _context.Events.Remove(existingEvent);
                var success = await TrySaveChangesAsync();
                if(success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
