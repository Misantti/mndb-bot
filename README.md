# Movie Night Database Bot for Discord

A Discord bot to organize movie events for discord and keep a record of watched movies. Using PostgreSQL as a database so it is
required. The bot works a bit differently than normal Discord bots. In the config file you can set a channel that the user has to
have access to be able to use bot. If the user has access to that channel it can be used in any channel or DM (where the bot is
present). If user does not have access to the channel specified, bot replies that you don't have access to this bot.

Bot has its own roles for users. These roles are NOT the same as on the Discord server! When user first time says anything to bot
it checks if the user has access to the channel specified. If user does have access bot creates user information in the PostgreSQL
database.

TMDB API is used to extract movie information from the TMDB database. For this to work TMDB API key is required. It can be easily
acquired from https://www.themoviedb.org/ after creating account.

Bot is written with DSharpPlus 4.0 .NET wrapper and uses FluentValidation .NET library.

SQL script for creating database for the bot is included. (DatabaseInitialization.sql)

TODO:
- Change database to SQLite
- Better DbContext
- Grouped commands


## SUPPORTED COMMANDS

| User commands | Minimum role | Description |
| --- | --- | --- |
| !hello | User | Bot says hello back and users role. |
| !readme | User |Print this file as a DM. |
| !deleteme | User | Bot deletes user information from database. Confirmation required. |
| !allusers | Admin | Post admin or owner a list of users with some info. List is DM'd. |
| !changerole role username | Admin | Change the role of a user to given role.  |
| !deleteuser username | Admin | Delete the user. Confirmation required. |


| Event commands | Minimum role | Description |
| --- | --- | --- |
| !addevent YYYY-MM-DD HH:mm | Admin | Create a movie night event. |
| !events | User | List of all events with ID. |
| !event id or Date | User | List movies watched at that event or error if no movies are added to that event. |
| !next | User | Prints date and time for the next scheduled event. |
| !last | User | Prints date of last event and the movies watched. User can then easily rate them with reactions. |
| !eventmovie eventId movieId | Mod | Add a movie to an event. |
| !deleteevent eventId | Admin | Delete event. |

| Movie commands | Minimum role | Description |
| --- | --- | --- |
| !addmovie IMDB or TMDB url/code | Mod | Create movie to the database. Bot extract IMDB/TMDB code from url and prints out info from TMDB. If the movie is correct, user confirms the movie and it is added to the database.  If the current date is same or next as previously created event the watched date for movie is automatically set as that. |
| !search searchString | User | Search movies from database and TMDB. Results are DM'd. |
| !movie imdb-url or id | User | Prints movie information if found and the average of user reviews. User can then react to it to rate it if not yet rated. If not found prints some info from TMDB and says it is not watched yet. |
| !watched | User | Lists all the movies you have watched as DM. |
| !unwatched | User | List all the movies you have not scored or marked as ignored. Used to easily score every movie you have not yet scored. |
| !deletemovie id | Admin | Delete movie from database. Confirmation required. |

| Genre commands | Minimum role | Description |
| --- | --- | --- |
| !addgenres | Admin | Add all the current genres from TMDB. Then asks if user wants to add mojis for added genres. |
| !addemojis | Admin | Add all the missing emojis for genres. |
| !addgenre 'genre name' | Admin | Add a custom genre. |
| !emoji 'genre name' | Mod | Print current emoji for genre or change it. |
| !deletegenre 'genre name'	| Admin | Delete a genre. Can only delete custom genres. |
