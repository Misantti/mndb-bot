﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using mndb_bot.Classes;
using mndb_bot.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Commands
{
    public class EventCommands : GlobalCommands
    {
        [Command("addevent")]
        [Description("Add an event to database.")]
        public async Task AddEvent(CommandContext ctx, [Description("Time in format YYYY-MM-DD HH:mm.")][RemainingText] string dateString)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "mod", true))
                {
                    var successParseDateTime = DateTime.TryParseExact(dateString, "yyyy-MM-dd HH:mm", new CultureInfo("fi-FI"), DateTimeStyles.None, out DateTime time);
                    if (successParseDateTime)
                    {
                        Event userEvent = new()
                        {
                            Time = time,
                            UserId = commandUser.Id
                        };
                        var createdEvent = await repository.CreateEvent(userEvent);
                        if (createdEvent == null)
                        {
                            await ctx.Channel.SendMessageAsync($"That date already has an event scheduled.").ConfigureAwait(false);
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"Created event at {createdEvent.Time.ToString("yyyy-MM-dd")} {createdEvent.Time.ToString("HH:dd")} with id {createdEvent.Id}.").ConfigureAwait(false);
                        }

                    }
                    else
                    {
                        await ctx.Channel.SendMessageAsync($"Unable to parse date and time. Use format YYYY-MM-DD hh:mm.").ConfigureAwait(false);
                    }
                }
            }
        }
        [Command("next")]
        [Description("Show the next event scheduled.")]
        public async Task NextEvent(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var allEvents = await repository.GetEvents();
                if (allEvents == null)
                {
                    await ctx.Channel.SendMessageAsync($"No events in database.").ConfigureAwait(false);
                }
                else
                {
                    var nextEvents = allEvents.Where(item => item.Time >= DateTime.Now).ToList();
                    if (nextEvents.Count() == 0)
                    {
                        await ctx.Channel.SendMessageAsync($"Currently no events scheduled.").ConfigureAwait(false);
                    }
                    else
                    {
                        var next = nextEvents.OrderBy(item => item.Time).FirstOrDefault();
                        await ctx.Channel.SendMessageAsync($"Next event is scheduled for {next.Time.ToShortDateString()} at {next.Time.ToString("HH:dd")}.").ConfigureAwait(false);
                    }
                }
            }
        }
        [Command("last")]
        [Description("Show the last event.")]
        public async Task LastEvent(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var channel = await PrivateChannel(ctx);
                var allEvents = await repository.GetEvents();
                if (allEvents == null)
                {
                    await channel.SendMessageAsync($"No events in database.").ConfigureAwait(false);
                }
                else
                {
                    var lastEvents = allEvents.Where(item => item.Time < DateTime.Now).ToList();
                    if (lastEvents.Count() == 0)
                    {
                        await channel.SendMessageAsync($"No past events in the database.").ConfigureAwait(false);
                    }
                    else
                    {
                        var lastEvent = lastEvents.OrderByDescending(item => item.Time).FirstOrDefault();
                        var eventMovies = await repository.GetMoviesForEvent(lastEvent);
                        await channel.SendMessageAsync($"Last event held was {lastEvent.Time.ToString("yyyy-MM-dd")}.").ConfigureAwait(false);

                        var messageList = new List<Tuple<DiscordMessage, Movie>>();
                        foreach (Movie movie in eventMovies)
                        {
                            var messageString = await CreateExistingMovieText(ctx, movie).ConfigureAwait(false);
                            var message = await channel.SendMessageAsync(messageString).ConfigureAwait(false);
                            messageList.Add(new Tuple<DiscordMessage, Movie>(message, movie));

                        }
                        foreach (Tuple<DiscordMessage, Movie> item in messageList)
                        {
                            var doContinue = await CreateScoreEmojis(ctx, item.Item1, item.Item2).ConfigureAwait(false);
                            if (!doContinue)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        [Command("events")]
        [Description("List events.")]
        public async Task Events(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var allEvents = await repository.GetEvents();
                if (allEvents == null)
                {
                    await ctx.Channel.SendMessageAsync("No events in the database.").ConfigureAwait(false);
                }
                else
                {
                    List<Event> pastEvents = new();
                    List<Event> futureEvents = new();
                    foreach (Event item in allEvents)
                    {
                        if (item.Time < DateTime.Now)
                        {
                            pastEvents.Add(item);
                        }
                        else
                        {
                            futureEvents.Add(item);
                        }
                    }
                    if (pastEvents.Count > 0)
                    {
                        string messageString = "```ID\tDate\n";
                        foreach (Event item in pastEvents)
                        {
                            messageString += $"{item.Id}\t{item.Time.ToString("yyyy-MM-dd")}";
                            if (item != pastEvents.Last())
                            {
                                messageString += "\n";
                            }
                            else
                            {
                                messageString += "```";
                            }
                        }
                        await ctx.Channel.SendMessageAsync(messageString).ConfigureAwait(false);
                    }
                    if (futureEvents.Count > 0)
                    {
                        bool confirm;
                        if (pastEvents.Count == 0)
                        {
                            confirm = true;
                        }
                        else
                        {
                            confirm = await Confirm(ctx, "There are future event(s) scheduled. Would you like to list those? Type *yes* to list them.", "yes");
                        }
                        if (confirm)
                        {
                            string messageString = "```ID\tDate\n";
                            foreach (Event item in futureEvents)
                            {
                                messageString += $"{item.Id}\t{item.Time.ToString("yyyy-MM-dd")}";
                                if (item != futureEvents.Last())
                                {
                                    messageString += "\n";
                                }
                                else
                                {
                                    messageString += "```";
                                }
                            }
                            await ctx.Channel.SendMessageAsync(messageString).ConfigureAwait(false);
                        }
                    }
                }

            }
        }
        [Command("event")]
        [Description("View the event specified.")]
        public async Task Event(CommandContext ctx, [Description("ID number of event")] int id)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var eventToView = await repository.GetEvent(id);
                await ViewEvent(ctx, eventToView);
            }
        }
        [Command("event")]
        [Description("View the event specified.")]
        public async Task Event(CommandContext ctx, [Description("Date in format YYYY-MM-DD")][RemainingText] string dateString)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var successParseDate = DateTime.TryParseExact(dateString, "yyyy-MM-dd", new CultureInfo("fi-FI"), DateTimeStyles.None, out DateTime time);
                if (!successParseDate)
                {
                    await ctx.Channel.SendMessageAsync("Unable to parse event date.").ConfigureAwait(false);
                }
                else
                {
                    var allEvents = await repository.GetEvents();
                    var eventToView = allEvents.FirstOrDefault(item => item.Time.Date == time.Date);
                    await ViewEvent(ctx, eventToView);
                }

            }
        }
        [Command("eventmovie")]
        [Description("Add movie to event")]
        public async Task AddEventMovie(CommandContext ctx, [Description("ID of event")] int eventId, [Description("ID of movie.")] int movieId)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "mod", true))
                {
                    var userEvent = await repository.GetEvent(eventId);
                    var userMovie = await repository.GetMovie(movieId);

                    if (userEvent == null || userMovie == null)
                    {
                        await ctx.Channel.SendMessageAsync("Invalid movie or event").ConfigureAwait(false);
                    }
                    else
                    {
                        var success = await repository.SetEventForMovie(userMovie, userEvent);
                        if (success)
                        {
                            await ctx.Channel.SendMessageAsync($"Movie with id {userMovie.Id} added to event {userEvent.Id}.").ConfigureAwait(false);
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"Error occurred while adding an event for movie.").ConfigureAwait(false);
                        }
                    }
                }
            }
        }
        [Command("deleteevent")]
        [Description("Delete event. Owner/admin or creator of event required.")]
        public async Task DeleteEvent(CommandContext ctx, [Description("ID of event")] int eventID)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var eventToDelete = await repository.GetEvent(eventID);
                if (eventToDelete == null)
                {
                    await ctx.Channel.SendMessageAsync("No event found with that ID.").ConfigureAwait(false);
                }
                else if (await CheckRoleLevel(ctx, "admin", true) || commandUser.Id == eventToDelete.UserId)
                {
                    string messageText = await CreateEventText(ctx, eventToDelete).ConfigureAwait(false);
                    messageText += "Are you sure you want to delete this event? Type *yes* to confirm";
                    var confirm = await Confirm(ctx, messageText, "yes").ConfigureAwait(false);

                    if(!confirm)
                    {
                        await ctx.Channel.SendMessageAsync($"Event deletion aborted.").ConfigureAwait(false);
                    }
                    else
                    {
                        var success = await repository.DeleteEvent(eventToDelete);
                        if (!success)
                        {
                            await ctx.Channel.SendMessageAsync($"Error while deleting event.").ConfigureAwait(false);
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"Event with ID {eventID} is now deleted.").ConfigureAwait(false);
                        }
                    }
                }
            }
        }
        private async Task ViewEvent(CommandContext ctx, Event eventToView)
        {
            if (eventToView == null)
            {
                await ctx.Channel.SendMessageAsync("No such event found.").ConfigureAwait(false);
            }
            else
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                var movies = await repository.GetMoviesForEvent(eventToView);

                if(movies == null)
                {
                    await channel.SendMessageAsync($"```Event {eventToView.Id} is scheluded for {eventToView.Time.ToString("yyyy-MM-dd HH:dd")}.\nNo movies added to the event.```").ConfigureAwait(false);
                }
                else
                {
                    await channel.SendMessageAsync($"Event {eventToView.Id} Date: {eventToView.Time.ToString("yyyy-MM-dd")}\nTotal movies watched: {movies.Count()}").ConfigureAwait(false);

                    var messageList = new List<Tuple<DiscordMessage, Movie>>();
                    foreach (Movie movie in movies)
                    {
                        var messageString = await CreateExistingMovieText(ctx, movie).ConfigureAwait(false);
                        var message = await channel.SendMessageAsync(messageString).ConfigureAwait(false);
                        messageList.Add(new Tuple<DiscordMessage, Movie>(message, movie));

                    }
                    foreach (Tuple<DiscordMessage, Movie> item in messageList)
                    {
                        var doContinue = await CreateScoreEmojis(ctx, item.Item1, item.Item2).ConfigureAwait(false);
                        if (!doContinue)
                        {
                            break;
                        }
                    }
                }

            }
        }
        private async Task<string> CreateEventText(CommandContext ctx, Event e)
        {
            var movies = await repository.GetMoviesForEvent(e);
            var eventId = e.Id;
            var date = e.Time.ToString("yyyy-MM-dd");
            string messageString = $"```Event {eventId} {date}\n";
            if (movies == null)
            {
                messageString += "No movies added to this event.```";
            }
            else
            {
                messageString += "ID    Name                              MNDB Score\n";
                messageString += "--------------------------------------------------\n";
                foreach (Movie movie in movies)
                {
                    var score = await repository.GetScore(movie);
                    string tempString = $"{movie.Id}";
                    while (tempString.Length < 6)
                    {
                        tempString += " ";
                    }
                    tempString += $"{movie.Name}";
                    if (score != 0)
                    {
                        while (tempString.Length < 43)
                        {
                            tempString += " ";
                        }
                        tempString += Convert.ToString(score);
                    }
                    messageString += tempString;
                    if (movie != movies.Last())
                    {
                        messageString += "\n";
                    }
                    else
                    {
                        messageString += "```";
                    }
                }
            }
            return messageString;
        }
    }
}
