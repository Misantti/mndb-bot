﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Entities;
using mndb_bot.Classes;
using mndb_bot.Models;
using System.Threading.Tasks;

namespace mndb_bot.Commands
{
    public class GenreCommands : GlobalCommands
    {
        [Command("addgenres")]
        [Description("Adds all the current genres from TMDB and saves them to database. Owner/admin only.")]
        public async Task AddGenres(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {

                    var tuple = await AddGenresFromTMDB(ctx);
                    var httpResultCode = tuple.Item1;
                    var createdGenres = tuple.Item2;

                    if (httpResultCode == System.Net.HttpStatusCode.OK)
                    {
                        if (createdGenres > 0)
                        {
                            await ctx.Channel.SendMessageAsync($"Added total of {createdGenres} missing genres from TMDB.").ConfigureAwait(false);
                            var addEmojis = await Confirm(ctx, "Would you like to add emojis to added genres? Type *yes* to add.", "yes").ConfigureAwait(false);
                            if (addEmojis)
                            {
                                await AddEmojis(ctx);
                            }
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"No missing genres. None added.").ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        await ctx.Channel.SendMessageAsync($"Error while connecting to TMDB.").ConfigureAwait(false);
                    }

                }
            }
        }
        [Command("addgenre")]
        [Description("Add a custom genre. Owner/admin only")]
        public async Task AddGenre(CommandContext ctx, [Description("Name of genre")] string genreName)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    var tuple = await AddGenresFromTMDB(ctx);
                    var httpResultCode = tuple.Item1;
                    if (await repository.GetGenreByName(genreName) != null)
                    {
                        await ctx.Channel.SendMessageAsync($"Genre already exists.").ConfigureAwait(false);
                    }
                    else
                    {
                        if (httpResultCode == System.Net.HttpStatusCode.OK)
                        {
                            var genre = new Genre();
                            genre.Name = genreName;
                            var success = await repository.CreateGenre(genre);
                            if (success)
                            {
                                await ctx.Channel.SendMessageAsync($"Genre created.").ConfigureAwait(false);
                            }
                            else
                            {
                                await ctx.Channel.SendMessageAsync($"Error creating genre to database.").ConfigureAwait(false);
                            }
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"Error while connecting to TMDB.").ConfigureAwait(false);
                        }

                    }


                }
            }
        }
        [Command("deletegenre")]
        [Description("Delete a custom genre. Owner/admin only")]
        public async Task DeleteGenre(CommandContext ctx, [Description("Name of genre")] string genreName)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    if(string.IsNullOrEmpty(genreName))
                    {
                        await ctx.Channel.SendMessageAsync($"You must type in a genre name.").ConfigureAwait(false);
                    }
                    else
                    {
                        var genreToDelete = await repository.GetGenreByName(genreName);
                        var tmdbClient = CreateTmdbClient(ctx);
                        var tuple = await tmdbClient.GetGenres();
                        var httpResultCode = tuple.Item1;
                        var TmdbGenres = tuple.Item2;
                        bool TmdbGenre = false;
                        if (httpResultCode == System.Net.HttpStatusCode.OK)
                        {
                            foreach (Genre genre in TmdbGenres)
                            {
                                if (genreToDelete.Id == genre.Id)
                                {
                                    TmdbGenre = true;
                                    break;
                                }
                            }
                            if (TmdbGenre)
                            {
                                await ctx.Channel.SendMessageAsync($"Unable to delete genres from TMDB.").ConfigureAwait(false);
                            }
                            else
                            {
                                var delete = await Confirm(ctx, $"Type *confirm* to delete genre {genreToDelete.Name}.", "confirm").ConfigureAwait(false);
                                if (delete)
                                {
                                    await repository.DeleteGenre(genreToDelete);
                                    await ctx.Channel.SendMessageAsync($"Genre deleted.").ConfigureAwait(false);
                                }
                            }
                        }
                        else
                        {
                            await ctx.Channel.SendMessageAsync($"Error while connecting to TMDB.").ConfigureAwait(false);
                        }
                    }


                }
            }
        }

        [Command("addemojis")]
        [Description("Add missing emojis for genres. Minimum role: mod.")]
        public async Task AddEmojis(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "mod", true))
                {
                    int emojisAdded = 0;
                    var genreList = await repository.GetGenres();
                    foreach (Genre genre in genreList)
                    {
                        if (string.IsNullOrEmpty(genre.Emoji))
                        {
                            var message = await ctx.RespondAsync($"{ctx.User.Mention}, please react to this message with emoji for genre {genre.Name}.").ConfigureAwait(false);
                            var response = await message.WaitForReactionAsync(ctx.User).ConfigureAwait(false);
                            if (!response.TimedOut)
                            {
                                await repository.GenreChangeEmoji(genre, response.Result.Emoji.GetDiscordName());
                                emojisAdded++;
                                await ctx.Channel.SendMessageAsync($"Emoji for {genre.Name} is now {response.Result.Emoji.GetDiscordName()}").ConfigureAwait(false);
                                
                            }
                            else
                            {
                                await ctx.Channel.SendMessageAsync($"Adding emojis timed out. Total of {emojisAdded} emojis added.").ConfigureAwait(false);
                                emojisAdded = -1;
                                break;
                            }
                        }
                    }
                    if (emojisAdded > 0)
                    {
                        await ctx.Channel.SendMessageAsync($"Total number of emojis added: {emojisAdded}").ConfigureAwait(false);
                    }
                    else if (emojisAdded == 0)
                    {
                        await ctx.Channel.SendMessageAsync($"None of the genres are missing emojis.").ConfigureAwait(false);
                    }

                }
            }
        }
        [Command("emoji")]
        [Description("View and/or change emoji for genre. Minimum role: mod.")]
        public async Task Emoji(CommandContext ctx, [Description("Name of genre")] string genreName)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "mod", true))
                {
                    var genre = await repository.GetGenreByName(genreName);
                    if (genre == null)
                    {
                        await ctx.Channel.SendMessageAsync($"Unable to find genre '{genreName}'.").ConfigureAwait(false);
                    }
                    else
                    {
                        var emojiExists = DiscordEmoji.TryFromName(ctx.Client, genre.Emoji, out DiscordEmoji currentEmoji);
                        DiscordMessage message;
                        if (emojiExists)
                        {
                            message = await ctx.Channel.SendMessageAsync($"Current emoji for genre '{genreName}' is {currentEmoji.Name}\nReact to this message with a new emote to overwrite.").ConfigureAwait(false);
                            
                        }
                        else
                        {
                            message = await ctx.Channel.SendMessageAsync($"'{genreName}' does not currently have emoji or it is invalid.\nReact to this message with a new emoji to change.").ConfigureAwait(false);
                        }
                        var response = await message.WaitForReactionAsync(ctx.User).ConfigureAwait(false);
                        if (!response.TimedOut)
                        {
                            var success = await repository.GenreChangeEmoji(genre, response.Result.Emoji.GetDiscordName());
                            if(success)
                            {
                                DiscordEmoji.TryFromName(ctx.Client, genre.Emoji, out DiscordEmoji newEmoji);
                                await ctx.Channel.SendMessageAsync($"Emoji for '{genre.Name}' is now {newEmoji.Name}").ConfigureAwait(false);
                            }

                        }

                    }

                }
            } 
        }

    }
}
