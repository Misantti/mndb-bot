﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.Configuration;
using mndb_bot.Models;
using mndb_bot.Repositories;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using DSharpPlus.Entities;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using System;
using System.Collections.Generic;

namespace mndb_bot.Classes
{
    public class GlobalCommands : BaseCommandModule
    {
        public IConfiguration configuration { get; set; }
        public IDbRepository repository { get; set; }
        public async Task<User> CheckUser(CommandContext ctx)
        {
            await ctx.TriggerTypingAsync().ConfigureAwait(false);

            var user = await repository.GetUserByDiscordID(ctx.User.Id);
            if (user != null)
            {
                return user;
            }
            else
            {
                ulong[] requiredChannelIDs = configuration.GetSection("Bot_Settings:Required_Access_To_Channel").Get<ulong[]>();

                List<DiscordChannel> requiredChannels = new();

                foreach(ulong channelID in requiredChannelIDs)
                {
                    var channel = await ctx.Client.GetChannelAsync(channelID).ConfigureAwait(false);
                    requiredChannels.Add(channel);
                }

                foreach(DiscordChannel channel in requiredChannels)
                {
                    var member = await channel.Guild.GetMemberAsync(ctx.User.Id).ConfigureAwait(false);
                    if (member != null)
                    {
                        if (member.PermissionsIn(channel).HasFlag(DSharpPlus.Permissions.AccessChannels))
                        {
                            string role;
                            if (ctx.User.Id == configuration.GetValue<ulong>("Bot_Settings:Owner_ID"))
                            {
                                role = "owner";
                                await ctx.Channel.SendMessageAsync("Owner found.").ConfigureAwait(false);
                            }
                            else
                            {
                                role = "user";
                            }
                            User newUser = new()
                            {
                                DiscordId = ctx.User.Id,
                                Username = ctx.User.Username,
                                Role = role
                            };
                            await repository.CreateUser(newUser);
                            return newUser;
                        }
                    }
                }
                await ctx.Channel.SendMessageAsync("Sorry, you don't have permission to access this bot. :(").ConfigureAwait(false);
                return null;
            }
        }
        public async Task<bool> IsRole(ulong discordID, string role)
        {
            var user = await repository.GetUserByDiscordID(discordID);
            if (user.Role != role || user == null)
                return false;
            else
                return true;

        }
        public async Task<int> GetRoleLevel(CommandContext ctx)
        {
            var user = await repository.GetUserByDiscordID(ctx.User.Id);
            if (user != null)
            {
                var role = user.Role;
                switch (role)
                {
                    case "user":
                        return 0;
                    case "mod":
                        return 1;
                    case "admin":
                        return 2;
                    case "owner":
                        return 3;
                    default:
                        return -1;
                }
            }
            return -1;
        }
        public async Task<bool> CheckRoleLevel(CommandContext ctx, int requiredLevel, bool message)
        {
            int level = await GetRoleLevel(ctx);
            if (level >= requiredLevel)
            {
                return true;
            }
            else
            {
                if (message)
                {
                    await ctx.Channel.SendMessageAsync($"You don't have permission to use this command.").ConfigureAwait(false);
                }
                return false;
            }
        }
        public async Task<bool> CheckRoleLevel(CommandContext ctx, string requiredRole, bool message)
        {
            int level = await GetRoleLevel(ctx);
            int requiredLevel;
            switch (requiredRole)
            {
                case "user":
                    requiredLevel = 0;
                    break;
                case "mod":
                    requiredLevel = 1;
                    break;
                case "admin":
                    requiredLevel = 2;
                    break;
                case "owner":
                    requiredLevel = 3;
                    break;
                default:
                    requiredLevel = -1;
                    break;
            }
            if (level >= requiredLevel)
            {
                return true;
            }
            else
            {
                if (message)
                {
                    await ctx.Channel.SendMessageAsync($"You don't have permission to use this command.").ConfigureAwait(false);
                }
                return false;
            }
        }
        public static bool FindImdbCode(string originalText, out string imdbCode)
        {
            if (!string.IsNullOrEmpty(originalText))
            {
                string text = originalText;
                int index_1;
                int index_2;
                do
                {
                    index_1 = text.IndexOf("tt");
                    if (index_1 != -1 && text.Length > index_1 + 2)
                    {
                        text = text.Substring(index_1 + 2);
                        index_2 = text.IndexOf("/");
                        string tempText;
                        if (index_2 != -1)
                        {
                            tempText = text.Remove(index_2);

                        }
                        else
                        {
                            tempText = text;
                        }
                        if (int.TryParse(tempText, out int value))
                        {
                            imdbCode = "tt" + tempText;
                            return true;
                        }
                    }
                } while (index_1 != -1);
            }
            imdbCode = null;
            return false;
        }
        public static bool FindTmdbCode(string originalText, out int tmdbCode)
        {
            if (!string.IsNullOrEmpty(originalText))
            {
                string[] numbers = Regex.Split(originalText, @"\D+");
                if (!string.IsNullOrEmpty(numbers[0]))
                {
                    tmdbCode = int.Parse(numbers[0]);
                    return true;
                }
            }
            tmdbCode = 0;
            return false;
        }
        public static string CreateImdbAddress(string id)
        {
            return "https://www.imdb.com/title/" + id + "/";
        }
        public static async Task<bool> Confirm(CommandContext ctx, string question, string confirmation, DiscordChannel channel = null)
        {
            if (channel == null)
            {
                channel = ctx.Channel;
            }
            await channel.SendMessageAsync(question).ConfigureAwait(false);

            var intr = ctx.Client.GetExtension<InteractivityExtension>();

            var result = await intr
                .WaitForMessageAsync(
                    m =>
                    m.Channel.Id == ctx.Channel.Id
                    && m.Author.Id == ctx.User.Id)
                .ConfigureAwait(false);

            if (!result.TimedOut)
            {
                if (Regex.IsMatch(result.Result.Content, confirmation))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else return false;
        }
        public async Task<DiscordChannel> PrivateChannel(CommandContext ctx)
        {
            if (ctx.Channel.IsPrivate)
            {
                return ctx.Channel;
            }
            else
            {
                var channel = await ctx.Member.CreateDmChannelAsync().ConfigureAwait(false);
                return channel;
            }
        }
        public TmdbApi CreateTmdbClient(CommandContext ctx)
        {
            return new TmdbApi(ctx.Services.GetService<IHttpClientFactory>(), configuration.GetValue<string>("TMDB_Settings:TMDB_API_key"));
        }
        public async Task<Tuple<HttpStatusCode, int>> AddGenresFromTMDB(CommandContext ctx)
        {
            var tmdbClient = CreateTmdbClient(ctx);
            var tuple = await tmdbClient.GetGenres();
            var httpStatusCode = tuple.Item1;
            var genreList = tuple.Item2;

            if (httpStatusCode == System.Net.HttpStatusCode.OK)
            {
                int createdGenres = 0;
                foreach (Genre genre in genreList)
                {
                    var created = await repository.CreateGenre(genre);
                    if (created)
                    {
                        createdGenres++;
                    }
                }
                return new Tuple<HttpStatusCode, int>(httpStatusCode, createdGenres);
            }

            return new Tuple<HttpStatusCode, int>(httpStatusCode, 0);
        }
        public async Task<string> CreateExistingMovieText(CommandContext ctx, Movie movie)
        {
            var TmdbClient = CreateTmdbClient(ctx);
            var tmdbMovie = await TmdbClient.FindMovieByTmdb(movie.Tmdb).ConfigureAwait(false);
            string messageText = $"```MNDB ID: {movie.Id}\t\t{movie.Name}\n";
            messageText += $"--------------------------------------\n";

            var averageScore = await repository.GetScore(movie);
            var userScore = await repository.GetUserScore(await repository.GetUserByDiscordID(ctx.User.Id), movie);
            var eventList = await repository.GetMoviesEvents(movie);
            if (averageScore != 0 || userScore != null || eventList != null)
            {
                if (averageScore != 0)
                {
                    messageText += $"Average score: {averageScore} / 4\n";
                }
                if (userScore != null)
                {
                    if (userScore.ScoreVal == -1)
                    {
                        messageText += $"You have not seen this movie.\n";
                    }
                    else if (userScore.ScoreVal == 0)
                    {
                        messageText += $"You were too drunk to remember this movie...\n";
                    }
                    else
                    {
                        messageText += $"Your score: {userScore.ScoreVal} / 4\n";
                    }
                    
                }
                if (eventList != null)
                {
                    messageText += "Movie was watched on event(s):\n";
                    foreach (Event e in eventList)
                    {
                        messageText += $"\t{e.Id}\t{e.Time.ToString("yyyy-MM-dd")}\n";

                    }
                }
                messageText += "--------------------------------------\n";
            }

            var genres = await repository.GetMoviesGenres(movie);
            if (genres != null)
            {
                foreach (Genre genre in genres)
                {
                    messageText += $"{genre.Name}\n";
                }
                messageText += "--------------------------------------\n";
            }
            if (tmdbMovie.Item1 == System.Net.HttpStatusCode.OK)
            {

                messageText += $"Release date: {tmdbMovie.Item2.release_date}\n";
                messageText += $"Plot synopsis: {tmdbMovie.Item2.overview}";
            }
            messageText += $"```";
            return messageText;
        }
        public string CreateTmdbMovieText(Movie_Results tmdb)
        {
            if (tmdb != null)
            {
                string messageText = $"```TMDB ID: {tmdb.id}\t\t{tmdb.title}\n";
                messageText += "--------------------------------------\n";

                messageText += $"Release date: {tmdb.release_date}\nPlot synopsis: {tmdb.overview}```";
                return messageText;
            }
            else
            {
                return null;
            }

        }
        public string CreateTmdbMovieText(Result tmdb)
        {
            if (tmdb != null)
            {
                string messageText = $"```TMDB ID: {tmdb.id}\t\t{tmdb.title}\n";
                messageText += "--------------------------------------\n";

                messageText += $"Release date: {tmdb.release_date}\nPlot synopsis: {tmdb.overview}```";
                return messageText;
            }
            else
            {
                return null;
            }

        }
        public async Task<bool> CreateScoreEmojis(CommandContext ctx, DiscordMessage message, Movie movie)
        {
            var commandUser = await repository.GetUserByDiscordID(ctx.User.Id);

            var channel = await PrivateChannel(ctx);

            var emojiXExists = DiscordEmoji.TryFromName(ctx.Client, ":x:", out DiscordEmoji xEmoji);
            var emojiZeroExists = DiscordEmoji.TryFromName(ctx.Client, ":beer:", out DiscordEmoji zeroEmoji);
            var emojiOneExists = DiscordEmoji.TryFromName(ctx.Client, ":one:", out DiscordEmoji oneEmoji);
            var emojiTwoExists = DiscordEmoji.TryFromName(ctx.Client, ":two:", out DiscordEmoji twoEmoji);
            var emojiThreeExists = DiscordEmoji.TryFromName(ctx.Client, ":three:", out DiscordEmoji threeEmoji);
            var emojiFourExists = DiscordEmoji.TryFromName(ctx.Client, ":four:", out DiscordEmoji fourEmoji);

            if (!emojiXExists || !emojiZeroExists || !emojiOneExists || !emojiTwoExists || !emojiThreeExists || !emojiFourExists)
            {
                return false;
            }

            await message.CreateReactionAsync(xEmoji);
            await message.CreateReactionAsync(zeroEmoji);
            await message.CreateReactionAsync(oneEmoji);
            await message.CreateReactionAsync(twoEmoji);
            await message.CreateReactionAsync(threeEmoji);
            await message.CreateReactionAsync(fourEmoji);

            var response = await message.WaitForReactionAsync(ctx.User).ConfigureAwait(false);

            if (response.TimedOut)
            {
                return false;
            }
            else
            {
                var score = new Score
                {
                    MovieId = movie.Id,
                    UserId = commandUser.Id
                };
                if (response.Result.Emoji == xEmoji)
                {
                    score.ScoreVal = -1;
                }
                else if (response.Result.Emoji == zeroEmoji)
                {
                    score.ScoreVal = 0;
                }
                else if (response.Result.Emoji == oneEmoji)
                {
                    score.ScoreVal = 1;
                }
                else if (response.Result.Emoji == twoEmoji)
                {
                    score.ScoreVal = 2;
                }
                else if (response.Result.Emoji == threeEmoji)
                {
                    score.ScoreVal = 3;
                }
                else if (response.Result.Emoji == fourEmoji)
                {
                    score.ScoreVal = 4;
                }
                else
                {
                    return false;
                }
                await repository.SetUserScore(score);
                await channel.SendMessageAsync($"Score for {movie.Name} is now {score.ScoreVal}.").ConfigureAwait(false);
                return true;
            }
        }
    }
}