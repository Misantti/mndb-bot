﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using mndb_bot.Classes;
using mndb_bot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mndb_bot.Commands
{
    public class MovieCommands : GlobalCommands
    {
        [Command("addmovie")]
        [Description("Add a movie to database")]
        public async Task AddMovie(CommandContext ctx, [Description("IMDB/TMDB address for the movie or just the code")]string codeString)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var isImdb = FindImdbCode(codeString, out string imdbCode);
                var isTmdb = FindTmdbCode(codeString, out int tmdbCode);
                if (!isImdb && !isTmdb)
                {
                    await ctx.Channel.SendMessageAsync($"Unable to parse movie code from {codeString}").ConfigureAwait(false);
                }
                else
                {
                    var TmdbClient = CreateTmdbClient(ctx);
                    if (isImdb)
                    {
                        var tupleImdb = await TmdbClient.FindMovieByImdb(imdbCode);
                        if (tupleImdb.Item1 != System.Net.HttpStatusCode.OK)
                        {
                            isTmdb = false;
                            await ctx.Channel.SendMessageAsync($"Unable to find movie by IMDB code, sorry...").ConfigureAwait(false);
                        }
                        else
                        {
                            isTmdb = true;
                            tmdbCode = tupleImdb.Item2.id;
                        }
                    }
                    if (isTmdb)
                    {
                        var tupleTmdb = await TmdbClient.FindMovieByTmdb(tmdbCode);
                        var tmdbMovie = tupleTmdb.Item2;

                        if (tupleTmdb.Item1 != System.Net.HttpStatusCode.OK)
                        {
                            await ctx.Channel.SendMessageAsync($"Unable to find movie by TMDB, sorry...").ConfigureAwait(false);
                        }
                        else
                        {
                            var existingMovie = await repository.GetMovieByTMDB(tmdbMovie.id);
                            if (existingMovie != null)
                            {
                                string message = "Movie already exists in the database.\n";
                                message += await CreateExistingMovieText(ctx, existingMovie);
                                await ctx.Channel.SendMessageAsync(message).ConfigureAwait(false);
                            }
                            else
                            {
                                var confirmed = await Confirm(ctx, $"Is this the right movie? Type *yes* to confirm.\n```{tmdbMovie.title}\n{tmdbMovie.release_date}\n{tmdbMovie.overview}```", "yes");
                                if(!confirmed)
                                {
                                    await ctx.Channel.SendMessageAsync($"Movie creation aborted.").ConfigureAwait(false);
                                }
                                else
                                {
                                    var movie = new Movie
                                    {
                                        Name = tmdbMovie.title,
                                        Imdb = tmdbMovie.imdb_id,
                                        Tmdb = tmdbMovie.id,
                                        CreatedAt = DateTime.Now,
                                        UserId = commandUser.Id
                                    };
                                    movie = await repository.CreateMovie(movie);
                                    if (movie == null)
                                    {
                                        await ctx.Channel.SendMessageAsync($"Error while saving movie to database.").ConfigureAwait(false);
                                    }
                                    else
                                    {
                                        foreach (TmdbGenre genre in tmdbMovie.genres)
                                        {
                                            var mg = new MoviesGenres
                                            {
                                                GenreId = genre.id,
                                                MovieId = movie.Id
                                            };
                                            if (await repository.GetGenre(genre.id) == null)
                                            {
                                                await AddGenresFromTMDB(ctx);
                                            }
                                            await repository.AddGenreToMovie(mg);
                                        }
                                        var existingEventToday = await repository.GetEventByDate(DateTime.Now);
                                        var existingEventYesterday = await repository.GetEventByDate(DateTime.Now.AddDays(-1));
                                        if (existingEventToday == null && existingEventYesterday == null)
                                        {
                                            await ctx.Channel.SendMessageAsync($"Added movie to database.").ConfigureAwait(false);
                                        }
                                        else
                                        {
                                            string date;
                                            Event currentEvent;
                                            if (existingEventToday != null)
                                            {
                                                date = "today";
                                                currentEvent = existingEventToday;
                                            }
                                            else
                                            {
                                                date = "yesterday";
                                                currentEvent = existingEventYesterday;
                                            }
                                            var confirm = await Confirm(ctx, $"There is an event scheduled for {date}. Would you like to add this movie to the event? Type *yes* to confirm.", "yes").ConfigureAwait(false);
                                            if (confirm)
                                            {
                                                await repository.SetEventForMovie(movie, currentEvent);
                                                await ctx.Channel.SendMessageAsync($"Added movie to database and to event {currentEvent.Id}.").ConfigureAwait(false);
                                            }

                                        }

                                    }
                                }

                            }
                            
                        }
                    }
                }
            }
        }
        [Command("search")]
        [Description("Search for movie and show information about it. Results are DM'd.")]
        public async Task SearchMovie(CommandContext ctx, [Description("Search this")][RemainingText] string searchString)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                var allMovies = await repository.GetMovies();
                var searchResults = allMovies.Where(item => item.Name.ToLower().Contains(searchString.ToLower())).ToList();
                bool confirmSearchFromTMDB = false;

                if (searchResults.Count() == 0)
                {
                    confirmSearchFromTMDB = await Confirm(ctx, $"Unable to find movie, sorry... Would you like to search from TMDB? Type *yes* to confirm.", "yes", channel).ConfigureAwait(false);
                }
                else
                {
                    foreach (Movie movie in searchResults)
                    {
                        var messageText = await CreateExistingMovieText(ctx, movie).ConfigureAwait(false);
                        var message = await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                        _ = CreateScoreEmojis(ctx, message, movie).ConfigureAwait(false);
                    }
                    confirmSearchFromTMDB = await Confirm(ctx, $"Would you also like to search from TMDB? Type *yes* to confirm.", "yes", channel).ConfigureAwait(false);
                }
                if (confirmSearchFromTMDB)
                {
                    var TmdbClient = CreateTmdbClient(ctx);
                    var tuple = await TmdbClient.SearchMovie(searchString);
                    var httpStatusCode = tuple.Item1;
                    var tmdbSearchResults = tuple.Item2;


                    if (httpStatusCode != System.Net.HttpStatusCode.OK || tmdbSearchResults.total_results == 0)
                    {
                        await channel.SendMessageAsync($"Unable to find movie from TMDB, sorry...").ConfigureAwait(false);
                    }
                    else
                    {
                        bool tooMany = false;
                        int count = tmdbSearchResults.total_results;
                        if (tmdbSearchResults.total_results > 10)
                        {
                            tooMany = true;
                            tmdbSearchResults.total_results = 10;
                        }
                        for (int i = 0; i < tmdbSearchResults.total_results; i++)
                        {
                            var existingMovie = await repository.GetMovieByTMDB(tmdbSearchResults.results[i].id);

                            if (existingMovie == null)
                            {
                                var messageText = CreateTmdbMovieText(tmdbSearchResults.results[i]);
                                await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                            }
                        }
                        if (tooMany)
                        {
                            await channel.SendMessageAsync("More results found, please refine search if your movie was not found.").ConfigureAwait(false);
                        }
                    }
                }
            }
        }
        [Command("movie")]
        [Description("Get a specific movie by ID or IMDB code/address. Results are DM'd.")]
        public async Task GetMovieByID(CommandContext ctx, [Description("ID or IMDB code of movie")][RemainingText] string text)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);

                var isImdb = FindImdbCode(text, out string imdbCode);
                if (isImdb)
                {
                    var movie = await repository.GetMovieByIMDB(imdbCode);
                    if (movie != null)
                    {
                        var messageText = await CreateExistingMovieText(ctx, movie).ConfigureAwait(false);
                        var message = await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                        _ = CreateScoreEmojis(ctx, message, movie);
                    }
                    else
                    {
                        var tmdbClient = CreateTmdbClient(ctx);
                        var tmdbTuple = await tmdbClient.FindMovieByImdb(imdbCode).ConfigureAwait(false);
                        if (tmdbTuple.Item1 == System.Net.HttpStatusCode.OK)
                        {
                            var tmdbMovie = tmdbTuple.Item2;
                            var messageText = "Movie does not currently exist in MNDB database.\n";
                            messageText += CreateTmdbMovieText(tmdbMovie);
                            await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                        }
                    }

                }
                else if (int.TryParse(text, out int ID))
                {
                    var movie = await repository.GetMovie(ID);
                    if (movie != null)
                    {
                        string messageText = await CreateExistingMovieText(ctx, movie);
                        var message = await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                        _ = CreateScoreEmojis(ctx, message, movie);
                    }
                }
                else
                {
                    await channel.SendMessageAsync($"Unable to parse movie code from {text}").ConfigureAwait(false);
                }

            }
        }
        [Command("watched")]
        [Description("Get a list of your watched movies. List is DM'd.")]
        public async Task WatchedMovies(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                var myScores = await repository.GetUserScores(commandUser);
                List<Movie> myMovies = new();

                foreach(Score score in myScores)
                {
                    myMovies.Add(await repository.GetMovie(score.MovieId));
                }
                if(myMovies.Count() == 0)
                {
                    await channel.SendMessageAsync("No movies found. The database may be down or you have not rated any movies.").ConfigureAwait(false);
                }
                else
                {
                    await channel.SendMessageAsync($"Total of {myMovies.Count()} movies watched").ConfigureAwait(false);
                    foreach (Movie movie in myMovies)
                    {
                        var messageText = await CreateExistingMovieText(ctx, movie);
                        await channel.SendMessageAsync(messageText).ConfigureAwait(false);
                    }
                }
            }
        }
        [Command("unwatched")]
        [Description("Get a list of all the movies you have not seen or rated.")]
        public async Task UnwatchedMovies(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                var allMovies = await repository.GetMovies();
                if (allMovies == null)
                {
                    await channel.SendMessageAsync("No movies found. The database may be down or you have not rated any movies.").ConfigureAwait(false);
                }
                else
                {
                    var myScores = await repository.GetUserScores(commandUser);
                    List<Movie> unwatchedMovies = new();
                    foreach(Movie movie in allMovies)
                    {
                        if(myScores.FirstOrDefault(item => item.MovieId == movie.Id) == null)
                        {
                            unwatchedMovies.Add(movie);
                        }
                    }
                    if (unwatchedMovies.Count() == 0)
                    {
                        await channel.SendMessageAsync("You have no unscored movies.").ConfigureAwait(false);
                    }
                    else
                    {
                        var messageList = new List<Tuple<DiscordMessage, Movie>>();
                        foreach(Movie movie in unwatchedMovies)
                        {
                            var messageString = await CreateExistingMovieText(ctx, movie).ConfigureAwait(false);
                            var message = await channel.SendMessageAsync(messageString).ConfigureAwait(false);
                            messageList.Add(new Tuple<DiscordMessage, Movie>(message, movie));
                            
                        }
                        foreach(Tuple<DiscordMessage, Movie> item in messageList)
                        {
                            var doContinue = await CreateScoreEmojis(ctx, item.Item1, item.Item2).ConfigureAwait(false);
                            if (!doContinue)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        [Command("deletemovie")]
        [Description("Delete movie from database. Admin/owner only.")]
        public async Task DeleteMovie(CommandContext ctx, [Description("ID of movie to delete")] int movieID)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    var movieToDelete = await repository.GetMovie(movieID);
                    if (movieToDelete == null)
                    {
                        await ctx.Channel.SendMessageAsync($"No movie found with ID {movieID}.").ConfigureAwait(false);
                    }
                    else
                    {
                        string messageString = await CreateExistingMovieText(ctx, movieToDelete).ConfigureAwait(false);
                        messageString += "\nAre you sure you want to delete this movie? Type *yes* to confirm.";
                        var confirm = await Confirm(ctx, messageString, "yes").ConfigureAwait(false);

                        if (!confirm)
                        {
                            await ctx.Channel.SendMessageAsync($"Movie deletion aborted.").ConfigureAwait(false);
                        }
                        else
                        {
                            var success = await repository.DeleteMovie(movieToDelete);
                            if (!success)
                            {
                                await ctx.Channel.SendMessageAsync($"Error while deleting movie.").ConfigureAwait(false);
                            }
                            else
                            {
                                await ctx.Channel.SendMessageAsync($"Movie is now deleted.").ConfigureAwait(false);
                            }
                        }
                    }
                }
            }
        }

    }
}
