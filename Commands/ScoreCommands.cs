﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using mndb_bot.Classes;
using mndb_bot.Models;
using System.Threading.Tasks;

namespace mndb_bot.Commands
{
    public class ScoreCommands : GlobalCommands
    {
        [Command("score")]
        [Description("Give score to a movie")]
        public async Task ScoreMovie(CommandContext ctx, [Description("Movie ID")] int movieId, [Description("Score value of 1-4 (0 is too drunk to remember and -1 is marked as unwatched.)")] short rating)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                var movie = await repository.GetMovie(movieId);
                if (movie == null || rating < -1 || rating > 4)
                {
                    string messageText = "";

                    if (movie == null)
                    {
                        messageText += "Unable to find movie.";
                    }
                    if (rating < -1 || rating > 4)
                    {
                        if (movie == null)
                        {
                            messageText += "\n";
                        }
                        messageText += "Invalid rating for a movie, only values between -1 and 4 accepted.";

                    }
                    await ctx.Channel.SendMessageAsync(messageText).ConfigureAwait(false);
                }
                else
                {
                    Score userScore = new()
                    {
                        UserId = commandUser.Id,
                        MovieId = movie.Id,
                        ScoreVal = rating
                    };
                    var success = await repository.SetUserScore(userScore);
                    if (success)
                    {
                        await ctx.Channel.SendMessageAsync("User score set.").ConfigureAwait(false);
                    }
                    else
                    {
                        await ctx.Channel.SendMessageAsync("Error whole setting score.").ConfigureAwait(false);
                    }
                    
                }
            }
        }
    }
}
