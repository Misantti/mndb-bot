﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using mndb_bot.Models;
using mndb_bot.Classes;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System;
using System.Collections.Generic;

namespace mndb_bot.Commands
{
    public class UserCommands : GlobalCommands
    {
        [Command("hello")]
        [Description("Say hello to the bot! Bot says hello back and prints your current role.")]
        public async Task Hello(CommandContext ctx)
        {
            var user = await CheckUser(ctx);
            if (user != null)
            {
                await ctx.Channel.SendMessageAsync($"Hello {user.Username}! Your current role is {user.Role}.").ConfigureAwait(false);
            }
        }
        [Command("readme")]
        [Description("Print out current readme as DM.")]
        public async Task ReadMe(CommandContext ctx)
        {
            var user = await CheckUser(ctx);
            string readme = "";
            if (user != null)
            {
                var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                bool fileRead;
                try
                {
                    readme = await File.ReadAllTextAsync(Path.Combine(Directory.GetCurrentDirectory(), "README.md")).ConfigureAwait(false);
                    fileRead = true;
                }
                catch(Exception ex)
                {
                    fileRead = false;
                    await channel.SendMessageAsync($"Unable to find readme file, sorry!").ConfigureAwait(false);
                    Console.WriteLine(ex.ToString());
                }
                if(fileRead)
                {
                    var readmeCharArray = readme.ToCharArray();
                    List<string> readmeList = new();
                    readmeList.Add("```");
                    int j = 0;
                    for (int i = 0; i < readmeCharArray.Length; i++)
                    {
                        readmeList[j] += readmeCharArray[i];
                        if (readmeList[j].Length > 1900 && readmeCharArray[i].Equals('\n'))
                        {
                            readmeList[j] += "```";
                            readmeList.Add("```");
                            j++;
                        }
                    }
                    readmeList[j] += "```";
                    foreach(string str in readmeList)
                    {
                        await channel.SendMessageAsync(str).ConfigureAwait(false);
                    }
                    
                }

            }
        }
        [Command("changerole")]
        [Description("Change role of a user. Requires owner/admin role to use.")]
        public async Task ChangeRole(CommandContext ctx, [Description("Name of role (admin, mod, user")]string role, [Description("Name of user whose role you want to change.")][RemainingText] string userName)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    var user = await repository.GetUserByName(userName);
                    if (user == null)
                    {
                        await ctx.Channel.SendMessageAsync($"User not found.").ConfigureAwait(false);
                    }
                    else
                    {
                        bool roleFound = false;
                        string[] validRoles = new string[] { "admin", "mod", "user" };
                        foreach (string validRole in validRoles)
                        {
                            if (role == validRole)
                            {
                                roleFound = true;
                                user = await repository.ChangeRole(user, role);
                                await ctx.Channel.SendMessageAsync($"Role for user {user.Username} is now {user.Role}.").ConfigureAwait(false);
                                break;
                            }
                        }
                        if (!roleFound)
                            await ctx.Channel.SendMessageAsync($"Role is not valid.").ConfigureAwait(false);
                    }
                }
                else
                {
                    await ctx.Channel.SendMessageAsync($"You don't have permission to use this command.").ConfigureAwait(false);
                }
            }
        }
        [Command("deleteme")]
        [Description("Delete your user data from the bot. Confirmation required.")]
        public async Task DeleteMe(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);

            if(commandUser != null)
            {
                var confirmed = await Confirm(ctx, "If you are sure that you want to delete your data, respond with *confirm* to continue.", "confirm").ConfigureAwait(false);

                if (confirmed)
                {
                    await repository.DeleteUser(commandUser);
                    await ctx.Channel.SendMessageAsync("User data deleted.").ConfigureAwait(false);
                }
                else
                {
                    await ctx.Channel.SendMessageAsync("Not confirmed.").ConfigureAwait(false);
                }
            }

        }
        [Command("allusers")]
        [Description("Get a list of all users. Owner/Admin only. List is DM'd.")]
        public async Task ListAllUsers(CommandContext ctx)
        {
            var commandUser = await CheckUser(ctx);
            if(commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    var channel = await PrivateChannel(ctx).ConfigureAwait(false);
                    var allUsers = await repository.GetUsers();
                    string messageString = "```ID\tName\t\t\tRole\n";
                    messageString += "----------------------------\n";
                    foreach (User user in allUsers)
                    {
                        string tempString = "";

                        tempString += $"{user.Id}";
                        while (tempString.Length < 6)
                        {
                            tempString += " ";
                        }
                        tempString += user.Username;
                        while (tempString.Length < 22)
                        {
                            tempString += " ";
                        }
                        tempString += user.Role;

                        messageString += tempString;

                        if (user != allUsers.Last())
                        {
                            messageString += "\n";
                        }
                        else
                        {
                            messageString += "```";
                        }
                    }
                    await channel.SendMessageAsync(messageString).ConfigureAwait(false);
                }

            }
        }
        [Command("deleteuser")]
        [Description("Delete all user data for user. Owner/Admin only.")]
        public async Task DeleteUser(CommandContext ctx, [Description("Username to delete")][RemainingText] string userName)
        {
            var commandUser = await CheckUser(ctx);
            if (commandUser != null)
            {
                if (await CheckRoleLevel(ctx, "admin", true))
                {
                    var userToDelete = await repository.GetUserByName(userName);
                    if(userToDelete == null)
                    {
                        await ctx.Channel.SendMessageAsync($"No user found with name {userName}.").ConfigureAwait(false);
                    }
                    else if (userToDelete.Role == "owner")
                    {
                        await ctx.Channel.SendMessageAsync($"Unable to delete owner.").ConfigureAwait(false);
                    }
                    else
                    {
                        var confirm = await Confirm(ctx, $"Are you sure you want to delete user {userToDelete.Username}? Type *yes* to confirm.", "yes").ConfigureAwait(false);
                        if(!confirm)
                        {
                            await ctx.Channel.SendMessageAsync($"User deletion aborted.").ConfigureAwait(false);
                        }
                        else
                        {
                            var success = await repository.DeleteUser(userToDelete);
                            if(!success)
                            {
                                await ctx.Channel.SendMessageAsync($"Error while deleting user.").ConfigureAwait(false);
                            }
                            else
                            {
                                await ctx.Channel.SendMessageAsync($"Userdata for {userName} is now deleted.").ConfigureAwait(false);
                            }
                        }
                    }
                }
            }
        }
    }
}
