﻿using System.Collections.Generic;

namespace mndb_bot.Models
{
    public partial class Genre
    {
        public Genre()
        {
            MoviesGenres = new HashSet<MoviesGenres>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Emoji { get; set; }
        public virtual ICollection<MoviesGenres> MoviesGenres { get; set; }
    }
}
