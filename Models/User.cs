﻿using System.Collections.Generic;

namespace mndb_bot.Models
{
    public partial class User
    {
        public User()
        {
            Movies = new HashSet<Movie>();
            Scores = new HashSet<Score>();
            Events = new HashSet<Event>();
        }

        public int Id { get; set; }
        public ulong DiscordId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
        public virtual ICollection<Score> Scores { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
