﻿namespace mndb_bot.Models
{
    public partial class Score
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public short ScoreVal { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual User User { get; set; }
    }
}
