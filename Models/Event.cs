﻿using System;
using System.Collections.Generic;

namespace mndb_bot.Models
{
    public partial class Event
    {
        public Event()
        {
            EventsMovies = new HashSet<EventMovies>();
        }
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<EventMovies> EventsMovies { get; set; }
    }
}