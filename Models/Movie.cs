﻿using System;
using System.Collections.Generic;

namespace mndb_bot.Models
{
    public partial class Movie
    {
        public Movie()
        {
            MoviesGenres = new HashSet<MoviesGenres>();
            EventsMovies = new HashSet<EventMovies>();
            Scores = new HashSet<Score>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Imdb { get; set; }
        public int Tmdb { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UserId { get; set; }
        public virtual ICollection<MoviesGenres> MoviesGenres { get; set; }
        public virtual ICollection<EventMovies> EventsMovies { get; set; }
        public virtual ICollection<Score> Scores { get; set; }
    }
}
