﻿namespace mndb_bot.Models
{

    public class TmdbGenres
    {
        public TmdbGenre[] genres { get; set; }
    }

    public class TmdbGenre
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}
