﻿namespace mndb_bot.Models
{
    public class EventMovies
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int EventId { get; set; }

        public virtual Event e { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
