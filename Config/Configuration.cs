﻿namespace mndb_bot.Config
{
    public class Configuration
    {
        public BotSettings Bot_Settings { get; set; }
        public TMDBSettings TMDB_Settings { get; set; }
        public PostgresqlSettings Postgresql_Settings { get; set; }
        public class BotSettings
        {
            public string Token { get; set; } = "Your secret bot token.";
            public long Owner_ID { get; set; } = 0;
            public string Prefix { get; set; } = "!";
            public long[] Required_Access_To_Channel { get; set; } = { 0, 1 };
        }

        public class TMDBSettings
        {
            public string TMDB_API_key { get; set; } = "Your TMDB API key.";
            public string TMDB_Read_Access_Token { get; set; } = "TMDB API 4 token, not needed in current version";
        }

        public class PostgresqlSettings
        {
            public string Connect_Options { get; set; } = "Server=Server name;Host=localhost;Port=5432;Username=postgres;Password=secretPassword;Database=database name";
        }
    }
}
