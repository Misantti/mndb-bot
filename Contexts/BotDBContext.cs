using Microsoft.EntityFrameworkCore;
using mndb_bot.Models;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace mndb_bot.Contexts
{
	public class BotDBContext : DbContext
	{
		public virtual DbSet<Genre> Genres { get; set; }
		public virtual DbSet<Movie> Movies { get; set; }
		public virtual DbSet<Score> Scores { get; set; }
		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<Event> Events { get; set; }
		public virtual DbSet<MoviesGenres> Movies_Genres { get; set; }
		public virtual DbSet<EventMovies> Event_Movies { get; set; }

		public BotDBContext()
		{
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var configuration = new ConfigurationBuilder()
				.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "config.json"), optional: false, reloadOnChange: false)
				.Build();
			optionsBuilder.UseNpgsql(configuration.GetValue<string>("PostgreSQL_Settings:Connect_Options"));
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Movie>().ToTable("movies");
			modelBuilder.Entity<Genre>().ToTable("genres");
			modelBuilder.Entity<User>().ToTable("users");
			modelBuilder.Entity<Score>().ToTable("scores");
			modelBuilder.Entity<Event>().ToTable("events");
			modelBuilder.Entity<MoviesGenres>().ToTable("movies_genres");
			modelBuilder.Entity<EventMovies>().ToTable("event_movies");
		}

    }

}