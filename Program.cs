﻿using mndb_bot.Classes;
using Newtonsoft.Json;
using System;
using System.IO;
using mndb_bot.Config;
using System.Threading.Tasks;
using mndb_bot.Validators;

namespace mndb_bot
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var configSuccess = await TryToValidateConfig(Path.Combine(Directory.GetCurrentDirectory(), "config.json"));

            if(configSuccess)
            {
                Console.WriteLine("Configuration file is valid, starting MNDB Bot.");
                Bot bot = new();
                try
                {
                    bot.RunAsync().GetAwaiter().GetResult();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                return;
            }
        }
        private static async Task<bool> TryToValidateConfig(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    Configuration config;
                    string json = await File.ReadAllTextAsync(path);
                    config = JsonConvert.DeserializeObject<Configuration>(json);
                    ConfigurationValidator validator = new();
                    var results = validator.Validate(config);
                    if(!results.IsValid)
                    {
                        foreach(var failure in results.Errors)
                        {
                            Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                        }
                    }
                    return results.IsValid;


                }
                else
                {
                    Configuration emptyConfigJson = new();
                    var configText = JsonConvert.SerializeObject(emptyConfigJson, Formatting.Indented);
                    try
                    {
                        Console.WriteLine("Config file does not exists, trying to create empty one.\nRemember to write your own settings to created config.json!");
                        File.WriteAllText(path, configText);
                        return false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
