﻿using FluentValidation;
using mndb_bot.Config;

namespace mndb_bot.Validators
{
    public class ConfigurationValidator : AbstractValidator<Configuration>
    {
        public ConfigurationValidator()
        {
            RuleFor(conf => conf.Bot_Settings.Owner_ID).NotEmpty();
            RuleFor(conf => conf.Bot_Settings.Token).NotEmpty();
            RuleFor(conf => conf.Bot_Settings.Prefix).NotEmpty();
            RuleFor(conf => conf.Bot_Settings.Required_Access_To_Channel).NotEmpty();

            RuleFor(conf => conf.TMDB_Settings.TMDB_API_key).NotEmpty();

            RuleFor(conf => conf.Postgresql_Settings.Connect_Options).NotEmpty()
                .Must(text => (
                    text.IndexOf("Server=") != -1
                    && text.IndexOf("Host=") != -1
                    && text.IndexOf("Port=5432") != -1
                    && text.IndexOf("Username=") != -1
                    && text.IndexOf("Password=") != -1
                    && text.IndexOf("Database=") != -1));
        }
    }
}