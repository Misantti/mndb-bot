﻿using FluentValidation;
using mndb_bot.Models;

namespace mndb_bot.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.DiscordId).NotNull();
            RuleFor(user => user.Username).NotNull().NotEmpty();
            RuleFor(user => user.Role)
                .Must(role =>
                    (role == "admin" || role == "owner" || role == "mod" || role == "user"));
        }
    }
}
