﻿using FluentValidation;
using mndb_bot.Models;
using System;

namespace mndb_bot.Validators
{
    public class MovieValidator : AbstractValidator<Movie>
    {
        public MovieValidator()
        {
            RuleFor(movie => movie.Imdb).NotEmpty().Must(imdb =>
                imdb.Remove(2) == "tt" && int.TryParse(imdb.Substring(2), out int result));
            RuleFor(movie => movie.Name).NotEmpty().Length(1, 255);
            RuleFor(movie => movie.Tmdb).NotEmpty();
            RuleFor(movie => movie.UserId).NotEmpty().GreaterThan(0);
            RuleFor(movie => movie.CreatedAt).NotEmpty().Must(item => item.GetType() == typeof(DateTime));
        }
        
    }
}
