﻿using FluentValidation;
using mndb_bot.Models;
using System;

namespace mndb_bot.Validators
{
    public class EventValidator : AbstractValidator<Event>
    {
        public EventValidator()
        {
            RuleFor(e => e.UserId).NotNull().GreaterThan(0);
            RuleFor(e => e.Time).NotEmpty().Must(item => item.GetType() == typeof(DateTime));
        }
    }
}
