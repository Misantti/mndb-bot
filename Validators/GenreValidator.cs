﻿using FluentValidation;
using mndb_bot.Models;

namespace mndb_bot.Validators
{
    public class GenreValidator : AbstractValidator<Genre>
    {
        public GenreValidator()
        {
            RuleFor(genre => genre.Name).NotEmpty().Length(2, 32);
            RuleFor(genre => genre.Emoji).Must(emoji => (
                emoji == null || (emoji.Length < 32 && emoji.Length >= 3)));
        }
    }
}
