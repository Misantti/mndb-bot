﻿using FluentValidation;
using mndb_bot.Models;

namespace mndb_bot.Validators
{
    public class MoviesGenresValidator : AbstractValidator<MoviesGenres>
    {
        public MoviesGenresValidator()
        {
            RuleFor(mg => mg.MovieId).NotEmpty().GreaterThan(0);
            RuleFor(mg => mg.GenreId).NotEmpty().GreaterThan(0);
        }
    }
}
