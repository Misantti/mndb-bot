﻿using FluentValidation;
using mndb_bot.Models;

namespace mndb_bot.Validators
{
    public class ScoreValidator : AbstractValidator<Score>
    {
        public ScoreValidator()
        {
            RuleFor(score => score.MovieId).NotEmpty().GreaterThan(0);
            RuleFor(score => score.UserId).NotEmpty().GreaterThan(0);
            RuleFor(score => score.ScoreVal).NotEmpty().Must(val =>
                (val >= -1 && val <= 4));
        }
    }
}
