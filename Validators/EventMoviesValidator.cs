﻿using FluentValidation;
using mndb_bot.Models;

namespace mndb_bot.Validators
{
    public class EventMoviesValidator : AbstractValidator<EventMovies>
    {
        public EventMoviesValidator()
        {
            RuleFor(eventMovies => eventMovies.MovieId).NotNull().GreaterThan(0);
            RuleFor(eventMovies => eventMovies.EventId).NotNull().GreaterThan(0);
        }
    }
}
