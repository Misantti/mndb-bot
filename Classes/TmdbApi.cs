﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using mndb_bot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace mndb_bot.Classes
{
    public class TmdbApi
    {
        private readonly IHttpClientFactory _iHttpClientFactory;
        private readonly string _tmdbApiKey;

        public TmdbApi(IHttpClientFactory httpClientFactory, string tmdbApiKey)
        {
            _iHttpClientFactory = httpClientFactory;
            _tmdbApiKey = tmdbApiKey;
        }
        private HttpClient CreateClient()
        {
            var httpClient = _iHttpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri("https://api.themoviedb.org/3/");
            return httpClient;
        }
        public async Task<Tuple<HttpStatusCode, Movie_Results>> FindMovieByImdb(string imdbID)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                $"find/{imdbID}?api_key={_tmdbApiKey}&external_source=imdb_id");

            var tuple = await TrySendAsync(request).ConfigureAwait(false);
            var httpStatusCode = tuple.Item1;
            
            if (httpStatusCode == HttpStatusCode.OK)
            {
                var jsonText = tuple.Item2;
                var json = JsonConvert.DeserializeObject<TmdbFindMovie>(jsonText);
                return new Tuple<HttpStatusCode, Movie_Results>(httpStatusCode, json.movie_results[0]);
            }
            else
            {
                return new Tuple<HttpStatusCode, Movie_Results>(httpStatusCode, null);
            }
        }
        public async Task<Tuple<HttpStatusCode, TmdbGetMovie>> FindMovieByTmdb(int tmdbID)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                $"movie/{tmdbID}?api_key={_tmdbApiKey}");

            var tuple = await TrySendAsync(request).ConfigureAwait(false);
            var httpStatusCode = tuple.Item1;
            
            if (httpStatusCode == HttpStatusCode.OK)
            {
                var jsonText = tuple.Item2;
                var json = JsonConvert.DeserializeObject<TmdbGetMovie>(jsonText);
                return new Tuple<HttpStatusCode, TmdbGetMovie>(httpStatusCode, json);
            }
            else
            {
                return new Tuple<HttpStatusCode, TmdbGetMovie>(httpStatusCode, null);
            }
        }
        public async Task<Tuple<HttpStatusCode, List<Genre>>> GetGenres()
        {
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                $"genre/movie/list?api_key={_tmdbApiKey}");

            var tuple = await TrySendAsync(request).ConfigureAwait(false);
            var httpStatusCode = tuple.Item1;

            if (httpStatusCode == HttpStatusCode.OK)
            {
                var jsonText = tuple.Item2;
                TmdbGenres json = JsonConvert.DeserializeObject<TmdbGenres>(jsonText);
                List<Genre> returnList = new();
                foreach (TmdbGenre tmdbGenre in json.genres)
                {
                    var genre = new Genre();
                    genre.Id = tmdbGenre.id;
                    genre.Name = tmdbGenre.name;
                    returnList.Add(genre);
                }
                return new Tuple<HttpStatusCode, List<Genre>>(httpStatusCode, returnList);
            }
            else
            {
                return new Tuple<HttpStatusCode, List<Genre>>(httpStatusCode, null);
            }
        }
        public async Task<Tuple<HttpStatusCode, TmdbSearchMovie>> SearchMovie(string searchString)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                $"search/movie?api_key={_tmdbApiKey}&query={searchString}&include_adult=false");

            var tuple = await TrySendAsync(request).ConfigureAwait(false);
            var httpStatusCode = tuple.Item1;
            
            if (httpStatusCode == HttpStatusCode.OK)
            {
                var jsonText = tuple.Item2;
                TmdbSearchMovie json = JsonConvert.DeserializeObject<TmdbSearchMovie>(jsonText);
                return new Tuple<HttpStatusCode, TmdbSearchMovie>(httpStatusCode, json);
            }
            else
            {
                return new Tuple<HttpStatusCode, TmdbSearchMovie>(httpStatusCode, null);
            }
        }
        private async Task<Tuple<HttpStatusCode, string>> TrySendAsync(HttpRequestMessage request)
        {
            var httpClient = CreateClient();
            HttpResponseMessage response;
            try
            {
                response = await httpClient.SendAsync(request).ConfigureAwait(false);
                string jsonText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return new Tuple<HttpStatusCode, string>(response.StatusCode, jsonText);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Tuple<HttpStatusCode, string>(HttpStatusCode.RequestTimeout, null);
            }
        }

    }
}
