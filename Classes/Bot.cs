﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using mndb_bot.Commands;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using mndb_bot.Contexts;
using mndb_bot.Repositories;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Interactivity.Enums;

namespace mndb_bot.Classes
{
    public class Bot
    {
        public IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration Configuration { get; private set; }

        public async Task RunAsync()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "config.json"), optional: false, reloadOnChange: true)
                .Build();

            ServiceProvider = new ServiceCollection()
                .AddSingleton(Configuration)
                .AddHttpClient()
                .AddDbContext<BotDBContext>()
                .AddSingleton<IDbRepository, DbRepository>()
                .BuildServiceProvider();

            var discordConfig = new DiscordConfiguration
            {
                Token = Configuration.GetValue<string>("Bot_Settings:Token"),
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                MinimumLogLevel = LogLevel.Debug,
                Intents = DiscordIntents.All
            };

            var commandsConfig = new CommandsNextConfiguration
            {
                StringPrefixes = new string[] { Configuration.GetValue<string>("Bot_Settings:Prefix") },
                EnableMentionPrefix = true,
                EnableDms = true,
                DmHelp = true,
                Services = ServiceProvider
            };

            var interactivityConfig = new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(30)
            };

            var discordClient = new DiscordShardedClient(discordConfig);


            await discordClient.UseInteractivityAsync(interactivityConfig);

            var commands = await discordClient.UseCommandsNextAsync(commandsConfig);
            commands.RegisterCommands<UserCommands>();
            commands.RegisterCommands<MovieCommands>();
            commands.RegisterCommands<GenreCommands>();
            commands.RegisterCommands<EventCommands>();
            commands.RegisterCommands<ScoreCommands>();

            try
            {
                await discordClient.StartAsync();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return;
            }
            

            await Task.Delay(-1);
        }
    }
}
